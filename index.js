/* eslint-disable no-console */

var express = require('express');
var config =  require('./config');
var app     = express();

app.set('port', config.web.port);

app.use(express.static(__dirname + '/public'));

app.listen(app.get('port'), function() {
  console.log('Server listening on port %s', app.get('port'));
});
