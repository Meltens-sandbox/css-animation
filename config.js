var config = {};
config.web = {};

config.web.port = process.env.PORT || 8000;

module.exports = config;
